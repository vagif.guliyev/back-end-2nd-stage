Good Morning. I am sorry that I could not pay your expectations.
--About Spring: If you research a bit, right now Spring Framework is the most popular 
Java Framework amoung enterprise ones. It is used to avoid useless, time-consuming codes.
In Spring only requirement is to write main ideas as simple as possible. Main Features:
1. Code Structure is very clear: Controller, Service, Entity, Repository and so on. 
2. Annotations make coding just fun. 
3. NoSQL coding: No need for creating table. Just connect it to proper database. Especially Joins are like magic.
4. application.properties file: where you can change any setting about you project.
5. Pom.XML: There bunch of Dependencies will make your coding easier and easier (for example Lombok)
6. It will automotically join and run perfectly the front-end applications.
Finally, about your friend, please ask managers. You can get more information from reception. 
Don't forget, whenever you want help, please find me on Slack or Campus. I would be glad to 
be helpful for you.