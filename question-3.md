Good Day, Lisa. Good to hear from you. 
Generally, Stream API will make your code more readable, clear and give you 
easier methods to code in a few seconds. No more For Loops and No more While Stuff.
-- To get clear understanding, please have a look at my github repo. You will
find many examples there step-by-step. (Alex's codes would be my best advice :) )
-- Try to read other developers code on Google and github, onlne pages like Baeldung or geeksforgeeks
would be great sources. First, try to write loops then convert them to streams. It will help you to understand.
-- Stream API is nothing but ready code pieces that was provided for you. You can
simply control+click any stream() method to see backstage of the methods. 
(Actually you will see simple codes there)
After all, please provide me with github link to your project and ask your questions.
If I see your code, then I can give some hints where you can use Stream API properly.